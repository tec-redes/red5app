package com.example.red5app;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.red5app.PublishFragment;

import com.red5pro.streaming.R5Connection;
import com.red5pro.streaming.R5Stream;
import com.red5pro.streaming.R5StreamProtocol;
import com.red5pro.streaming.config.R5Configuration;
import com.red5pro.streaming.source.R5Camera;
import com.red5pro.streaming.source.R5Microphone;
import com.red5pro.streaming.R5Stream.RecordType;
import com.red5pro.streaming.view.R5VideoView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SubscribeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SubscribeFragment extends Fragment {

    public static SubscribeFragment newInstance() {
        SubscribeFragment fragment = new SubscribeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public SubscribeFragment() {
        // Required empty public constructor
    }

    public R5Configuration configuration;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configuration = new R5Configuration(R5StreamProtocol.RTSP, "red5.centralus.cloudapp.azure.com",  8554, "live", 1.0f);
        configuration.setLicenseKey("AV55-NZQN-CNM8-B2CC");
        configuration.setParameters("username=username;password=username;");
        configuration.setBundleID(getActivity().getPackageName());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_subscribe, container, false);
        return v;
    }

    protected boolean isSubscribing = false;
    protected R5Stream stream;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Button publishButton = (Button) getActivity().findViewById(R.id.subscribeButton);
        publishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubscribeToggle();
            }
        });
    }

    private void onSubscribeToggle() {
        Button subscribeButton = (Button) getActivity().findViewById(R.id.subscribeButton);
        if(isSubscribing) {
            stop();
        }
        else {
            start();
        }
        isSubscribing = !isSubscribing;
        subscribeButton.setText(isSubscribing ? "stop" : "start");
    }

    public void start() {
        R5VideoView videoView = (R5VideoView) getActivity().findViewById(R.id.subscribeView);
        EditText editTextStream2 = (EditText) getActivity().findViewById(R.id.editTextStream2);

        stream = new R5Stream(new R5Connection(configuration));
        videoView.attachStream(stream);
        stream.play(editTextStream2.getText().toString());
    }

    public void stop() {
        if(stream != null) {
            stream.stop();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(isSubscribing) {
            onSubscribeToggle();
        }
    }
}